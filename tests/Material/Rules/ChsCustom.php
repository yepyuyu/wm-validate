<?php

namespace Itwmw\Validate\Tests\Material\Rules;

use Itwmw\Validate\Support\Rule\BaseRule;

class ChsCustom extends BaseRule
{
    /**
     * 默认错误消息.
     *
     * @var string
     */
    protected $message = ':attribute的值只能具有中文';

    /**
     * 确定验证规则是否通过。
     */
    public function passes($attribute, $value): bool
    {
        return is_scalar($value) && 1 === preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', (string) $value);
    }
}
