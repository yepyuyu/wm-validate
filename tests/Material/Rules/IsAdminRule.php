<?php

namespace Itwmw\Validate\Tests\Material\Rules;

use Itwmw\Validate\Support\Rule\BaseRule;

class IsAdminRule extends BaseRule
{
    protected $message = ':Attribute必须为admin';

    public function passes($attribute, $value): bool
    {
        return 'admin' === $value;
    }
}
