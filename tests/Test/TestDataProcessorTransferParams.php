<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Support\Concerns\ProcessorInterface;
use Itwmw\Validate\Support\Processor\ProcessorOptions;
use Itwmw\Validate\Support\Processor\ProcessorParams;
use Itwmw\Validate\Support\Processor\ProcessorValueType;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Validate;

class StringArrayToIntArrayProcessor implements ProcessorInterface
{
    public function __construct(
        protected string $separator = ','
    ) {
    }

    public function handle($value, string $attribute, array $originalData): array
    {
        return array_map('intval', explode($this->separator, $value));
    }
}

class TestDataProcessorTransferParams extends BaseTestValidate
{
    public function testProcessorFunction()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required|string'
            ];

            protected $postprocessor = [
                'id' => [
                    ProcessorOptions::MULTIPLE,
                    ['explode', ',', ProcessorParams::Value],
                    ['array_map', 'intval', ProcessorParams::Value],
                ],
            ];
        };

        $data = $v->check([
            'id' => '1,2,3,4,5'
        ]);

        $this->assertSame([1, 2, 3, 4, 5], $data['id']);
    }

    public function testProcessorMethod()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required|string'
            ];

            protected $postprocessor = [
                'id' => [
                    ProcessorOptions::MULTIPLE,
                    ['explode_method', ',', ProcessorParams::Value, ProcessorValueType::METHOD],
                    ['array_map', 'intval', ProcessorParams::Value],
                ],
            ];

            public function explodeMethodProcessor(string $separator, string $value): array
            {
                return explode($separator, $value);
            }
        };

        $data = $v->check([
            'id' => '1,2,3,4,5'
        ]);

        $this->assertSame([1, 2, 3, 4, 5], $data['id']);
    }

    public function testProcessorClass()
    {
        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required|string'
            ];

            protected $postprocessor = [
                'id' => [StringArrayToIntArrayProcessor::class, '-'],
            ];
        };

        $data = $v->check([
            'id' => '1-2-3-4-5'
        ]);

        $this->assertSame([1, 2, 3, 4, 5], $data['id']);

        $v = new class extends Validate {
            protected $rule = [
                'id' => 'required|string'
            ];

            protected $postprocessor = [
                'id' => StringArrayToIntArrayProcessor::class,
            ];
        };

        $data = $v->check([
            'id' => '1,2,3,4,5'
        ]);

        $this->assertSame([1, 2, 3, 4, 5], $data['id']);
    }
}
