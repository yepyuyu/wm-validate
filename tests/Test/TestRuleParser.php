<?php

namespace Itwmw\Validate\Tests\Test;

use Itwmw\Validate\Support\Traits\RuleParamsParser;
use Itwmw\Validate\Tests\Material\BaseTestValidate;
use Itwmw\Validate\Validate;

class TestRuleParserFunction
{
    public static function getPhpVersion(): string
    {
        return PHP_VERSION;
    }

    public static function getName(string $name, string $base_name): string
    {
        return $base_name . '_' . $name;
    }
}

class TestUser
{
    public array $info = [
        'name' => '虞灪'
    ];
}

class TestRuleParserValidator extends Validate
{
    use RuleParamsParser;

    protected array $ruleParams = [];

    protected string $name = '虞灪';

    protected TestUser $user;

    public $rule = [
        'name'                => 'required|string',
        'field'               => 'testRule:{:name},{:field}',
        'env'                 => 'testRule:{@PHP_VERSION}',
        'function'            => 'testRule:{#getNumberOne},2,{#Itwmw\Validate\Tests\Test\TestRuleParserFunction::getPhpVersion}',
        'property'            => 'testRule:{->name}',
        'multilevel_property' => 'testRule:{->user->info.name}',
        'functionParam'       => 'testRule:{#Itwmw\Validate\Tests\Test\TestRuleParserFunction::getName({:name},test)}',
    ];

    protected $scene = [
        'testField'              => ['name', 'field'],
        'testEnv'                => ['env'],
        'testFunction'           => ['function'],
        'testProperty'           => ['property'],
        'testMultilevelProperty' => ['multilevel_property'],
        'testFunctionParam'      => ['name', 'functionParam']
    ];

    public function initUser(): static
    {
        $this->user = new TestUser();
        return $this;
    }

    public function ruleTestRule($att, $value, $params): bool
    {
        $this->ruleParams = $params;
        return true;
    }

    public function getRuleParams(): array
    {
        return $this->ruleParams;
    }

    public function getNumberOne(): int
    {
        return 1;
    }
}

class TestRuleParser extends BaseTestValidate
{
    public function testParamsWithField()
    {
        $v = TestRuleParserValidator::make();
        $v->scene('testField')->check([
            'name'  => '虞灪',
            'field' => '字段'
        ]);

        $params = $v->getRuleParams();
        $this->assertEquals('虞灪', $params[0]);
        $this->assertEquals('字段', $params[1]);
    }

    public function testParamsWithEnv()
    {
        putenv('PHP_VERSION=' . PHP_VERSION);
        $v = TestRuleParserValidator::make();
        $v->scene('testEnv')->check([
            'env' => '1.0.0'
        ]);
        $params = $v->getRuleParams();
        $this->assertEquals(PHP_VERSION, $params[0]);
    }

    public function testParamsWithFunction()
    {
        $v = TestRuleParserValidator::make();
        $v->scene('testFunction')->check([
            'function' => true
        ]);
        $params = $v->getRuleParams();
        $this->assertEquals(1, $params[0]);
        $this->assertEquals(2, $params[1]);
        $this->assertEquals(PHP_VERSION, $params[2]);
    }

    public function testParamsWithProperty()
    {
        $v = TestRuleParserValidator::make();
        $v->scene('testProperty')->check([
            'property' => true
        ]);
        $params = $v->getRuleParams();
        $this->assertEquals('虞灪', $params[0]);
    }

    public function testParamsWithMultilevelProperty()
    {
        $v = TestRuleParserValidator::make();
        $v->scene('testMultilevelProperty')->initUser()->check([
            'multilevel_property' => true
        ]);
        $params = $v->getRuleParams();
        $this->assertEquals('虞灪', $params[0]);
    }

    public function testParamsWithFunctionAndParam()
    {
        $v = TestRuleParserValidator::make();
        $v->scene('testFunctionParam')->initUser()->check([
            'name'          => '虞灪',
            'functionParam' => true
        ]);
        $params = $v->getRuleParams();
        $this->assertEquals('test_虞灪', $params[0]);
    }
}
