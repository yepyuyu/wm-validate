<?php

namespace Itwmw\Validate\Providers\Laravel;

use Illuminate\Support\ServiceProvider;
use Itwmw\Validate\Support\Storage\ValidateConfig;
use Itwmw\Validation\Factory;

class ValidateProvider extends ServiceProvider
{
    public function boot()
    {
        $factory = new Factory(null, str_replace('-', '_', config('app.locale')));
        $factory->setPresenceVerifier(new PresenceVerifier($this->app->get('db')));
        ValidateConfig::instance()->setFactory($factory);
    }
}
