<?php

namespace Itwmw\Validate\Support\Processor;

enum ProcessorOptions implements ProcessorSupport
{
    /**
     * 当你需要定义多个处理器时，可以使用此选项
     */
    case MULTIPLE;
}
