<?php

namespace Itwmw\Validate\Support\Processor;

enum ProcessorExecCond: int implements ProcessorSupport
{
    /**
     * 当数据为空时
     *
     * null、空数组、空字符串都视为空，false不视为空
     */
    case WHEN_EMPTY = 1;

    /**
     * 当数据不为空时
     */
    case WHEN_NOT_EMPTY = 2;

    /**
     * 当数据存在时,无论是什么值
     */
    case WHEN_EXIST = 4;

    /**
     * 当数据存在且为空时
     */
    case WHEN_EXIST_AND_EMPTY = 5;

    /**
     * 当数据不存在时
     */
    case WHEN_NOT_EXIST = 8;

    /**
     * 任何情况都处理数据，包括空值
     *
     * 默认行为，通常不需要手动指定
     */
    case ALWAYS = 0;
}
