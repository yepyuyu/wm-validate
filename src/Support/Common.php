<?php

namespace Itwmw\Validate\Support;

class Common
{
    /**
     * Name of the generated error message.
     */
    public static function makeMessageName(string $fieldName, string $rule): string
    {
        if (str_contains($rule, ':')) {
            $rule = substr($rule, 0, strpos($rule, ':'));
        }
        return $fieldName . '.' . $rule;
    }

    /**
     * Get rules for a given field and populate non-existent fields.
     */
    public static function getRulesAndFill(array $rules, array $fields): array
    {
        $rules    = array_intersect_key($rules, array_flip($fields));
        $notExist = array_diff($fields, array_keys($rules));
        return array_merge($rules, array_fill_keys($notExist, []));
    }
}
