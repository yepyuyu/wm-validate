<?php

namespace Itwmw\Validate\Support\Event;

abstract class ValidateEventAbstract implements ValidateEventInterface
{
    /**
     * Current validation scene name.
     */
    public ?string $sceneName;

    /**
     * Current validated data.
     */
    public array $data;

    /**
     * Error message.
     */
    public string $message;

    public function beforeValidate(): bool
    {
        return true;
    }

    public function afterValidate(): bool
    {
        return true;
    }
}
