<?php

namespace Itwmw\Validate\Support;

use Itwmw\Validate\Support\Concerns\MessageProviderInterface;
use Itwmw\Validation\Support\Arr;
use Itwmw\Validation\Support\Str;

class MessageProvider implements MessageProviderInterface
{
    /**
     * The array of custom attribute names.
     */
    protected array $customAttributes = [];

    /**
     * The array of custom error messages.
     */
    protected array $messages = [];

    /**
     * Data for validate.
     */
    protected array $data = [];

    /**
     * Attribute Name.
     */
    protected string $attribute = '';

    /**
     * 关键词处理
     *
     * @var array<string, callable(array, array):string>
     */
    protected array $handler = [];

    /**
     * Indexing when validating array structures.
     */
    protected array $attributeIndex = [];

    protected string $rule = '';

    protected ?string $displayableAttribute = '';

    public function setDisplayableAttribute(?string $displayableAttribute): static
    {
        $this->displayableAttribute = $displayableAttribute;
        return $this;
    }

    public function setMessages(array $messages): static
    {
        $this->messages = $messages;
        return $this;
    }

    public function setCustomAttributes(array $customAttributes): static
    {
        $this->customAttributes = $customAttributes;
        return $this;
    }

    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getInitialMessage(?string $key = null, ?string $rule = null): string|array
    {
        if (is_null($rule)) {
            if (is_null($key)) {
                return $this->messages;
            } else {
                return $this->messages['key'] ?? '';
            }
        }

        $keyString = Common::makeMessageName($key, $rule);
        if (isset($this->messages[$keyString])) {
            return $this->messages[$keyString];
        }

        // 如果错误消息是由数组形式来定义的
        if (!empty($this->messages[$key])) {
            $message = $this->messages[$key];
            if (is_array($message)) {
                return $message[$rule] ?? '';
            }
        }

        return '';
    }

    public function handleMessage(string $messages): string
    {
        // 验证器没有找到对应的错误信息
        if (str_starts_with($messages, 'validation.')) {
            // 尝试将规则名转为首字母大写的驼峰式去查询
            $rule    = Str::studly($this->rule);
            $message = $this->getInitialMessage($this->attribute, $rule);
            if (empty($message)) {
                // 尝试将规则名转为首字母小写的驼峰式去查询
                $rule    = Str::camel($this->rule);
                $message = $this->getInitialMessage($this->attribute, $rule);
            }

            if (!empty($message)) {
                $messages = $message;
            }
        }
        return $this->replacingFieldsInMessage($messages);
    }

    public function setAttribute(string $attribute): static
    {
        $this->attribute = $attribute;
        if (preg_match_all('/\.(\d+)(?=\.|$)/', $attribute, $matches)) {
            $this->attributeIndex = $matches[1];
        } else {
            $this->attributeIndex = [];
        }

        return $this;
    }

    public function setRule(string $rule): static
    {
        $this->rule = $rule;
        return $this;
    }

    public function getMessage(string $key, ?string $rule = null): ?string
    {
        $error = $this->getInitialMessage($key, $rule);
        return $this->replacingFieldsInMessage($error);
    }

    /**
     * @param string                             $key
     * @param callable(array, array):string|null $using
     *
     * @return $this
     */
    public function render(string $key, ?callable $using): static
    {
        if (is_null($using)) {
            unset($this->handler[$key]);
            return $this;
        }

        if (!$using instanceof \Closure) {
            $using = $using(...);
        }

        $this->handler[$key] = $using;
        return $this;
    }

    /**
     * Replacing fields in error messages.
     *
     * @return string|string[]
     */
    protected function replacingFieldsInMessage(string $message): array|string
    {
        if (str_contains($message, ':input')) {
            $replace = Arr::get($this->data, $this->attribute, '');
            if (is_scalar($replace)) {
                $message = str_replace(':input', $replace, $message);
            }
        }

        if (!empty($this->displayableAttribute)) {
            $message = str_replace(
                [':attribute', ':ATTRIBUTE', ':Attribute'],
                [$this->displayableAttribute, Str::upper($this->displayableAttribute), Str::ucfirst($this->displayableAttribute)],
                $message
            );
        }

        if (preg_match_all('/{(&+)(\d+)}/', $message, $matches) > 0) {
            foreach ($matches[0] as $index => $pregString) {
                $_index         = $matches[2][$index];
                $attributeIndex = $this->attributeIndex[$_index] ?? 0;
                if ('&&' == $matches[1][$index]) {
                    ++$attributeIndex;
                }

                $message = str_replace($pregString, $attributeIndex, $message);
            }
        }

        if (preg_match_all('/{:(.*?)}/', $message, $matches) > 0) {
            foreach ($matches[0] as $index => $pregString) {
                $_index = 0;
                $key    = $matches[1][$index];
                while ($i = strpos($key, '*')) {
                    $attributeIndex = $this->attributeIndex[$_index] ?? 0;
                    $key            = substr_replace($key, $attributeIndex, $i, strlen($attributeIndex));
                    ++$_index;
                }
                $message = str_replace($pregString, Arr::get($this->data, $key, ''), $message);
            }
        }

        if (preg_match_all('/{@(.*?)}/', $message, $matches) > 0) {
            foreach ($matches[0] as $index => $pregString) {
                $message = str_replace($pregString, $this->customAttributes[$matches[1][$index]] ?? '', $message);
            }
        }

        foreach ($this->handler as $key => $handler) {
            if (str_contains($message, $key)) {
                $replace = $handler($this->data, $this->customAttributes);
                $message = str_replace($key, $replace, $message);
            }
        }

        return $message;
    }
}
