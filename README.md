<h2 align="center">微梦表单验证</h2>

<p align="center">
<a href="https://gitee.com/yepyuyu/wm-validate/stargazers"><img src="https://gitee.com/yepyuyu/wm-validate/badge/star.svg?theme=dark" alt="Star"></a>
<a href="https://gitee.com/yepyuyu/wm-validate/members"><img src="https://gitee.com/yepyuyu/wm-validate/badge/fork.svg?theme=dark" alt="Fork"></a>
</p>

<p align="center">
<a href="https://packagist.org/packages/itwmw/validate"><img src="http://poser.pugx.org/itwmw/validate/v" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/itwmw/validate"><img src="https://shields.io/badge/coverage-100%25-brightgreen" alt="Tests"></a>
<a href="https://packagist.org/packages/itwmw/validate"><img src="http://poser.pugx.org/itwmw/validate/license" alt="License"></a>
<a href="https://packagist.org/packages/itwmw/validate"><img src="http://poser.pugx.org/itwmw/validate/require/php" alt="PHP Version Require"></a>
</p>

中文 | 
[English](https://gitee.com/yepyuyu/wm-validate/blob/6.x/README-EN.md)

## 介绍
一个让你的表单验证更为方便，快捷，安全的扩展，满足你的一切验证需求。


## 目录
- [验证器](https://v.itwmw.com/6/Validate.html)
- [验证场景](https://v.itwmw.com/6/Scene.html)
- [场景事件](https://v.itwmw.com/6/Event.html)
- [内置规则](https://v.itwmw.com/6/BuiltRule.html)
- [自定义验证规则](https://v.itwmw.com/6/Rule.html)
- [自定义消息](https://v.itwmw.com/6/Message.html)
- [数据处理器](https://v.itwmw.com/6/Processor.html)
- [验证集合](https://v.itwmw.com/6/Collection.html)

## 安装
使用composer命令
``` shell
composer require itwmw/validate
```

完整文档查看[完整文档](https://v.itwmw.com)

## 简单验证
支持简单定义一个验证器并进行验证：
```php
try {
    $data = Validate::make([
        'user' => 'required|email',
        'pass' => 'required|lengthBetween:6,16',
    ], [
        'user.required'      => '请输入用户名',
        'user.email'         => '用户名格式错误',
        'pass.required'      => '请输入密码',
        'pass.lengthBetween' => '密码长度为6~16位',
    ])->check($data);
} catch (ValidateException $e) {
    echo $e->getMessage();
}
```
如果验证通过，则返回所有通过验证的值，如未通过，则抛出一个`Itwmw\Validate\Exception\ValidateException`异常

## 验证器定义
为具体的验证场景或者数据表单定义验证器类，我们需要继承`Itwmw\Validate\Validate`类，然后实例化后直接调用验证类的`check`方法即可完成验证，下面是一个例子：

我们定义一个`LoginValidate`验证器类用于登录的验证。
```php
class LoginValidate extends Validate
{
    protected $rule = [
        'user' => 'required|email',
        'pass' => 'required|digits_between:6,16',
    ];
    
    protected $message = [
        'user.required'       => '请输入用户名',
        'user.email'          => '用户名格式错误',
        'pass.required'       => '请输入密码',
        'pass.digits_between' => '密码长度为6~16位',
    ];
}

```

> <div style="padding-top:3px;color:#42b983">类属性定义的错误消息，优先级要高于自定义规则中的默认回复，高于自定义规则方法返回的错误</div>

## 数据验证
``` php
$data = [
    'user' => '123@qq.com',
    'pass' => ''
];
$validate = new LoginValidate();
$validate->check($data);
```
此时会抛出一个`Itwmw\Validate\Exception\ValidateException`异常，message为`请输入密码`
``` php
$data = [
    'user' => '123@qq.com',
    'pass' => '123456'
];
$validate = new LoginValidate();
$data = $validate->check($data);
```
验证成功，并返回通过验证的值，返回的值为数组类型

## 验证数组
验证表单的输入为数组的字段也不难。你可以使用 「点」方法来验证数组中的属性。例如，如果传入的 HTTP 请求中包含`search[keyword]`字段， 可以如下验证：
``` php
protected $rule = [
    'search.order'   => 'numeric|between:1,2',
    'search.keyword' => 'chsAlphaNum',
    'search.recycle' => 'boolean',
];
```
你也可以验证数组中的每个元素。例如，要验证指定数组输入字段中的每一个 id 是唯一的，可以这么做：
``` php
protected $rule = [
    'search.*.id' => 'numeric|unique:account'
];
```
数组规则的错误消息的定义也一样
``` php 
protected $message = [
    'search.order.numeric'       => '排序参数错误',
    'search.order.between'       => '排序参数错误',
    'search.keyword.chsAlphaNum' => '关键词只能包含中文，字母，数字',
    'search.recycle.boolean'     => '参数错误：recycle',
];
```
## 验证器类属性
### $rule
用户定义验证器的验证规则，也可以通过`setRules`方法来进行设置，此方法为叠加，如果参数为`null`则为清空全部规则
```php
// 类中定义
protected $rule = [
    'user' => 'required'
];

// 使用方法定义
$v->setRules([
    'user' => 'required'
]);
```

### $message
用户定义验证器的错误信息，也可以通过`setMessages`方法来进行设置，此方法为叠加，如果参数为`null`则为清空全部错误消息
```php
// 类中定义
protected $message = [
    'user.required' => '账号必须填写'
];

// 使用方法定义
$v->setMessages([
    'user.required' => '账号必须填写'
]);
```
更多用法可以查看[错误消息](https://v.itwmw.com/6/Message.html)一节

### $scene
定义验证场景的数据，用于指定验证场景对应的验证字段等，详细用法查看[验证场景](https://v.itwmw.com/6/Scene.html)一节,同样也可以通过`setScene`方法来进行设置，此方法为叠加，如果参数为`null`则为清空全部验证场景
```php
// 类中定义
protected $scene = [
    'login' => ['user', 'pass']
];

// 使用方法定义
$v->setScene([
    'login' => ['user', 'pass']
]);
```

### $event
定义此验证器下的全局事件，详细用法查看[事件](https://v.itwmw.com/6/Event.html)一节
```php
protected $event = [
    CheckSiteStatus::class
];
```

### $customAttributes
定义验证字段的名称,也可以通过`setCustomAttributes`方法来进行设置，此方法为叠加，如果参数为`null`则为清空全部字段名称，
错误消息中的[:attribute](https://v.itwmw.com/6/Message.html#attribute)会使用下面的值对应的替换
```php
protected $customAttributes = [
    'user' => '账号',
    'pass' => '密码'
];
```

### $preprocess
为字段定义一个前置数据处理器
```php
protected $preprocessor = [
    'name' => ['默认名称', ProcessorExecCond::WHEN_EMPTY]
];
```
关于前置数据处理器的详情请查看[数据处理器](https://v.itwmw.com/6/Processor.html)一节

### $postprocessor
为字段定义一个后置数据处理器
```php
protected $postprocessor = [
    'name' => ['trim', ProcessorExecCond::WHEN_NOT_EMPTY, ProcessorParams::Value]
];
```
关于后置数据处理器的详情请查看[数据处理器](https://v.itwmw.com/6/Processor.html)一节

### $ruleMessage
当前类中自定义方法验证失败后的错误消息
```php
 protected $ruleMessage = [
    ':attribute的值只能具有中文'
];
```
点击查看[示例](https://v.itwmw.com/6/Rule.html#%E4%BD%BF%E7%94%A8%E7%B1%BB%E6%96%B9%E6%B3%95)

### $regex
预定义正则表达式验证规则,详情查看[正则表达式规则](https://v.itwmw.com/6/Rule.html#%E6%AD%A3%E5%88%99%E8%A1%A8%E8%BE%BE%E5%BC%8F%E8%A7%84%E5%88%99)
```php
protected $regex = [
	'number' => '/^\d+$/'
];
```

### $group
定义验证规则组
```php
protected $group = [
    'username' => 'alpha_num|min:5|max:10'
];

protected $rule = [
    'username' => 'required|username'
];
```
也可以使用数组格式：
```php
protected $group = [
    'username' => [
        'alpha_num',
        'min:5',
        'max:10'
    ]
];

protected $rule = [
    'username' => 'required|username'
];
```

## 特别鸣谢

感谢 Jetbrains 提供的 IDE 开源许可。

[![](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg)](https://www.jetbrains.com/?from=https://gitee.com/yepyuyu/wm-validate)
