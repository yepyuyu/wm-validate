<h2 align="center">Micro Dream Form Validate</h2>

<p align="center">
<a href="https://gitee.com/yepyuyu/wm-validate/stargazers"><img src="https://gitee.com/yepyuyu/wm-validate/badge/star.svg?theme=dark" alt="Star"></a>
<a href="https://gitee.com/yepyuyu/wm-validate/members"><img src="https://gitee.com/yepyuyu/wm-validate/badge/fork.svg?theme=dark" alt="Fork"></a>
</p>

<p align="center">
<a href="https://packagist.org/packages/itwmw/validate"><img src="http://poser.pugx.org/itwmw/validate/v" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/itwmw/validate"><img src="https://shields.io/badge/coverage-100%25-brightgreen" alt="Tests"></a>
<a href="https://packagist.org/packages/itwmw/validate"><img src="http://poser.pugx.org/itwmw/validate/license" alt="License"></a>
<a href="https://packagist.org/packages/itwmw/validate"><img src="http://poser.pugx.org/itwmw/validate/require/php" alt="PHP Version Require"></a>
</p>


[中文](https://gitee.com/yepyuyu/wm-validate/blob/6.x/README.md) |
English

## Introduction
An extension to make your form validation easier, faster and more secure for all your validation needs.

## Catalog
- [Validator](https://v.itwmw.com/en/6/Validate.html)
- [Validate Scene](https://v.itwmw.com/en/6/Scene.html)
- [Validate Events](https://v.itwmw.com/en/6/Event.html)
- [Built-in Rules](https://v.itwmw.com/en/6/BuiltRule.html)
- [Custom Rules](https://v.itwmw.com/en/6/Rule.html)
- [Custom Error Messages](https://v.itwmw.com/en/6/Message.html)
- [Data Processor](https://v.itwmw.com/en/6/Processor.html)
- [Validate Collection](https://v.itwmw.com/en/6/Collection.html)

## Install
using Composer
``` shell
composer require itwmw/validate
```

View the complete document [here](https://v.itwmw.com).

## Simple Validate
Support for simply defining a validator and performing validation:
```php
try {
    $data = Validate::make([
        'user' => 'required|email',
        'pass' => 'required|lengthBetween:6,16',
    ], [
        'user.required'      => 'Please enter your username',
        'user.email'         => 'User name format error',
        'pass.required'      => 'Please enter your password',
        'pass.lengthBetween' => 'Password length is 6~16 bits',
    ])->check($data);
} catch (ValidateException $e) {
    echo $e->getMessage();
}
```
If the validation passes, all values that passed the validation are returned,
if not, a `Itwmw\Validate\Exception\ValidateException` exception is thrown

## Validator Definition
To define the validator class for a specific validation scenario or data form, we need to inherit the `Itwmw\Validate\Validate` class,
Then instantiate and call the `check` method of the validation class directly to complete the validation, an example of which is as follows.

Define a `LoginValidate` validator class for login validation.
```php
class LoginValidate extends Validate
{
    protected $rule = [
        'user' => 'required|email',
        'pass' => 'required|digits_between:6,16',
    ];
    
    protected $message = [
        'user.required'       => 'Please enter your username',
        'user.email'          => 'Incorrect username format',
        'pass.required'       => 'Please enter your password',
        'pass.digits_between' => 'Password length of 6 to 16 digits',
    ];
}

```
:::tip <div style="padding-top:3px;color:#42b983">Error messages defined by class attributes take precedence over the default responses in custom rules and over the errors returned by custom rule methods.</div>
:::

## Data Validate
``` php
$data = [
    'user' => '123@qq.com',
    'pass' => ''
];
$validate = new LoginValidate();
$validate->check($data);
```
This throws a `Itwmw\Validate\Exception\ValidateException` exception with the message `Please enter your password`
``` php
$data = [
    'user' => '123@qq.com',
    'pass' => '123456'
];
$validate = new LoginValidate();
$data = $validate->check($data);
```
Validates successfully and returns the validated value, which is an array type

## Validate Arrays
It is not difficult to validate the form input as an array of fields.
You can use the "dot" method to validate properties in an array.
For example, if the incoming HTTP request contains the `search[keyword]` field,
it can be validated as follows.

``` php
protected $rule = [
    'search.order'   => 'numeric|between:1,2',
    'search.keyword' => 'chsAlphaNum',
    'search.recycle' => 'boolean',
];
```
You can also validate each element in an array. For example, to validate that each id in a given array input field is unique, you can do this.

``` php
protected $rule = [
    'search.*.id' => 'numeric|unique:account'
];
```
The definition of an error message for an array rule is the same

``` php 
protected $message = [
    'search.order.numeric'       => 'order parameter error',
    'search.order.between'       => 'order parameter error',
    'search.keyword.chsAlphaNum' => 'Keywords can only contain Chinese, letters, numbers',
    'search.recycle.boolean'     => 'Parameter error:recycle',
];
```
## Validator Class Attributes
### $rule
The validation rules of the user-defined validator can also be set via the `setRules` method,
This method will superimpose the data.If the parameter is `null` then it is clear all rules.
```php
// Definition in class
protected $rule = [
    'user' => 'required'
];

// Definition of usage methods
$v->setRules([
    'user' => 'required'
]);
```

### $message
User-defined validator error messages can also be set via the `setMessages` method,
This method will superimpose the data,If the parameter is `null` then it is clear all error messages.
```php
// Definition in class
protected $message = [
    'user.required' => 'user is required'
];

// Definition of usage methods
$v->setMessages([
    'user.required' => 'pass is required'
]);
```
For more usage examples, please refer to the [Message](https://v.itwmw.com/en/6/Message.html) section.

### $scene
Define the data of the validation scenario, which is used to specify the validation fields corresponding to the validation scenario, etc.
See the section on [validate scene](https://v.itwmw.com/en/6/Scene.html) for detailed usage.

The same can be done with the `setScene` method.
This method will superimpose the data,
If the parameter is `null`, then all validate scene are cleared.
```php
// Definition in class
protected $scene = [
    'login' => ['user', 'pass']
];

// Definition of usage methods
$v->setScene([
    'login' => ['user', 'pass']
]);
```

### $event
Define global events under this validator, see the section [Events](https://v.itwmw.com/en/6/Event.html) for detailed usage.
```php
protected $handler = [
    CheckSiteStatus::class
];
```

### $preprocess
Defines a preprocessing handler for a field.
```php
protected $preprocessor = [
    'name' => ['Default Name', ProcessorExecCond::WHEN_EMPTY]
];
```
For more details about preprocessing handlers, please refer to the [Processor](https://v.itwmw.com/en/6/Processor.html) section.

### $postprocessor
Defines a postprocessing handler for a field.
```php
protected $postprocessor = [
    'name' => ['trim', ProcessorExecCond::WHEN_NOT_EMPTY, ProcessorParams::Value]
];
```
For more details about postprocessing handlers, please refer to the [Processor](https://v.itwmw.com/en/6/Processor.html) section.

### $customAttributes
Define the name of the validation field, also can be set by `setCustomAttributes` method, this method will superimpose the data,
if the parameter is `null` then it is clear all field names.
the [:attribute](https://v.itwmw.com/en/6/Message.html#attribute) in the error message will be replaced with the value corresponding to the following

```php
protected $customAttributes = [
    'user' => 'Account',
    'pass' => 'Password'
];
```
### $ruleMessage
The error message for failed validation of a custom method in the current class.
```php
 protected $ruleMessage = [
    'The value of :attribute can only have Chinese'
];
```
Click to view [example](https://v.itwmw.com/en/6/Rule.html#using-rule-objects)

### $filled
All validated fields cannot be empty when they exist, if the value of this property is `true`,
all rules will automatically add `filled` rules, the default is `true`

The `filled` rule is not automatically added when:
- Validation rules exist in `filled`, `nullable`, `accepted`, `declined`, `declined_if`, `present`, `required`, `required_if`, `required_unless`, `required_with`,`required_with_all`, `required_without`, `required_without_all` Rule
- Validation rules exist [extendImplicit](https://v.itwmw.com/en/6/Rule.html#extendimplicit) Defined rules
- The validation rules implement the `Itwmw\Validation\Support\Interfaces\ImplicitRule` Marker Interface
```php
protected bool $filled = true;
```

### $regex
Predefined regular expression validation rules, see [Regular Expression Rule](https://v.itwmw.com/en/6/Rule.html#regular-expression-rule) for details
```php
protected $regex = [
	'number' => '/^\d+$/'
];
```

### $group
Define validation rule groups
```php
protected $group = [
    'username' => 'alpha_num|min:5|max:10'
];

protected $rule = [
    'username' => 'required|username'
];
```
Can also use array format:
```php
protected $group = [
    'username' => [
        'alpha_num',
        'min:5',
        'max:10'
    ]
];

protected $rule = [
    'username' => 'required|username'
];
```
